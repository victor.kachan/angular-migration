//import { enableProdMode } from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {setAngularLib} from '@angular/upgrade/static';
import { UpgradeModule } from '@angular/upgrade/static';
import * as angular from 'angular';

import {AppModule} from './app/app.module';
import {uiComponentsModule} from './ng1/ui-components.module';

//enableProdMode();
setAngularLib(angular);

platformBrowserDynamic().bootstrapModule(AppModule).then(platformRef => {
    const upgrade = platformRef.injector.get(UpgradeModule) as UpgradeModule;
    upgrade.bootstrap(document.body, [uiComponentsModule.name], { strictDi: true });
});