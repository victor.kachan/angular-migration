import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {UpgradeModule} from '@angular/upgrade/static';

import {TestComponentFacade} from '../ng1/test/test.component';
import {HeaderComponentFacade} from '../ng1/header/main';

import {AppComponent} from './app.component';

import { HomeComponent } from './pages/home/home.component';
import { StyleguideComponent } from './pages/styleguide/styleguide.component';

import { routing } from './app.routing';

@NgModule({
    imports: [
        BrowserModule,
        UpgradeModule,
        routing
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        StyleguideComponent,
        TestComponentFacade,
        HeaderComponentFacade,
    ],
    entryComponents: [
        AppComponent
    ],
    bootstrap: []
})
export class AppModule {
    //constructor(*/private upgrade: UpgradeModule*/) {}

    ngDoBootstrap() {
        //this.upgrade.bootstrap(document.body, [uiComponentsModule.name]);
    }
}
