import {Component} from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'bva-app',
    templateUrl: './app.component.html'
})

export class AppComponent {
    static selector = 'bvaApp';
    name = 'world';
    constructor(router: Router) {
        router.initialNavigation();
    }
}
