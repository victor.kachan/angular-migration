import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { StyleguideComponent } from './pages/styleguide/styleguide.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'styleguide', component: StyleguideComponent }
];

export const routing = RouterModule.forRoot(routes);
