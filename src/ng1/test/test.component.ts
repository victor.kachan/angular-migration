import {Directive, ElementRef, Injector, Input} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';


export const testComponent = {
  selector: 'test',
  template: 'Hello, {{ $ctrl.userName }}!',
  bindings: {
      userName: '<'
  },
  controller: class TestComponent {
      userName: string;
  }
};

@Directive({selector: testComponent.selector})
export class TestComponentFacade extends UpgradeComponent {
  @Input() userName: string;
  
  constructor(elementRef: ElementRef, injector: Injector) {
    super(testComponent.selector, elementRef, injector);
  }
}
