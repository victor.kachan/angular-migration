// 'use strict';
//
// define(function (require, exports, module) {
//
//     /**
//      * @ngInject
//      */
//     module.exports = {
//         bindings: {},
//         template: require('./template.html'),
//         controller: require('./scripts/controller.js')
//     }
// });


import {Directive, ElementRef, Injector} from '@angular/core';
import {UpgradeComponent} from '@angular/upgrade/static';
import {HeaderComponent} from './scripts/controller';

export const headerComponent = {
    selector: 'bva-header',
    templateUrl: './template.html',
    bindings: {
    },
    controller: HeaderComponent
};

@Directive({selector: headerComponent.selector})
export class HeaderComponentFacade extends UpgradeComponent {
    constructor(elementRef: ElementRef, injector: Injector) {
        super(headerComponent.selector, elementRef, injector);
    }
}