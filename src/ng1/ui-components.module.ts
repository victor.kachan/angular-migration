import * as angular from 'angular';
import {downgradeComponent} from '@angular/upgrade/static';
import {AppComponent} from '../app/app.component';
import {testComponent} from './test/test.component';
import {headerComponent} from './header/main';


export const uiComponentsModule = angular.module('ui-components', [])
    .component(testComponent.selector, testComponent)
    .component(headerComponent.selector, headerComponent)
    .directive(AppComponent.selector, downgradeComponent({
        component: AppComponent
    }));
