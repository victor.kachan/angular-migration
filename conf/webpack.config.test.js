var path = require('path');
var merge = require('merge-deep');
var config = require('./webpack.config.js');

var extConfig = {
    output: {
        chunkFilename: '[name].bundle.js'
    },
    module: {
        rules: [
            // //HTML
            // {
            //     test: /(\.htm|\.html)$/,
            //     enforce: 'pre',
            //     loader: 'htmllint-loader?' + JSON.stringify({
            //         config: '.htmllintrc',
            //         failOnWarning: false,
            //         failOnError: true
            //     }),
            //     exclude: /node_modules|tests|bva-npm-modules/
            // },
            // // Javascript
            // {
            //     test: /\.js?$/,
            //     enforce: 'pre',
            //     loader: 'eslint-loader?' + JSON.stringify({
            //         failOnWarning: false,
            //         failOnError: true
            //     }),
            //     exclude: /node_modules|tests|bva-npm-modules/
            // }
        ]
    },
    devtool: '#eval',
    resolve: {
        alias: {
            'bva-config': path.resolve('./conf/config.acc.json'),
            'bva-app-mock': path.resolve('./../bva-app-mock')
        }
    }
};

module.exports = merge(config, extConfig);