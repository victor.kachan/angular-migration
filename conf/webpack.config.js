var path = require('path');
var webpack = require('webpack');

var CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;
var autoprefixer = require('autoprefixer');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var merge = require('merge-deep');
var ContextReplacementPlugin = require('webpack/lib/ContextReplacementPlugin');
var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

var DIST_FOLDER = path.resolve('./dist');

var atlOptions = '';

module.exports = {
    entry: {
        polyfills: [path.resolve('./src/polyfills.ts')],
        vendor: [path.resolve('./src/vendor.ts')],
        app: [path.resolve('./src/main.ts')]
    },
    output: {
        path: DIST_FOLDER,
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[chunkhash].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: ['awesome-typescript-loader?' + atlOptions, 'angular2-template-loader', '@angularclass/hmr-loader'],
                exclude: [/node_modules\/(?!(ng2-.+))/]
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
                exclude: [/locale\/([a-z]{2})\.json$/]
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader'})
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!resolve-url-loader!sass-loader?sourceMap'})
            },
            {
                test: /\.(jpg|jpeg|gif|png|svg|woff|woff2|ttf|eot|ico)(\?\S*)?$/,
                loader: 'file-loader?name=assets/[name].[ext]'
            },
            {
                test: /\.zip$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.spec\.js$/,
                loader: 'ignore-loader'
            },
            {
                test: /locale\/([a-z]{2})\.json$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.scss$/,
                loader: 'ignore-loader',
                exclude: [/main\.scss/]
            }
        ]
    },
    resolve: {
        modules: [
            __dirname,
            'node_modules'
        ],
        descriptionFiles: ['package.json', 'bower.json'],
        alias: merge(
            {/*'angular': path.resolve('./node_modules/angular')*/},
            require('./webpack.aliases.js')),
        extensions: ['.ts', '.js', '.json', '.css', '.scss', '.html']
    },
    postcss: [
        autoprefixer({
            browsers: ['last 2 version']
        })
    ],
    plugins: [
        //new ContextReplacementPlugin(/\/app\/$/, __dirname + '/app/', false),
        new ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)@angular/,
            path.resolve(__dirname, '../src')
        ),
        new ExtractTextPlugin({ filename: 'css/[name].css', disable: false, allChunks: true }),
        new CommonsChunkPlugin({
            name: ['vendor', 'polyfills']
        }),
        new webpack.ProvidePlugin({
            jQuery: 'jquery',
            $: 'jquery',
            jquery: 'jquery'
        }),
        new ngAnnotatePlugin({
            add: true
        }),
        new webpack.DefinePlugin({
            DEV: JSON.stringify(false)
        }),
        new HtmlWebpackPlugin({
            template: './src/index.html',
            // favicon: 'app/media/favicon.ico',
            chunksSortMode: 'dependency'
        })
    ]
};
