var path = require('path');
var merge = require('merge-deep');
var nodeExternals = require('webpack-node-externals');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var isCoverage = process.env.NODE_ENV === 'coverage';

module.exports = {
    context: path.resolve('./app/../'),
    output: {
        devtoolModuleFilenameTemplate        : '[absolute-resource-path]',
        devtoolFallbackModuleFilenameTemplate: '[absolute-resource-path]?[hash]'
    },
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
    module: {
        rules: [
            {
                test: /\.json$/,
                loader: 'json-loader',
                exclude: [/locale\/([a-z]{2})\.json$/]
            },
            {
                test: /\.html$/,
                loader: 'raw-loader'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader'})
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader!resolve-url-loader!sass-loader?sourceMap'})
            },
            {
                test: /\.(jpg|jpeg|gif|png|svg|woff|woff2|ttf|eot|ico)(\?\S*)?$/,
                loader: 'file-loader?name=assets/[name].[ext]'
            },
            {
                test: /\.zip$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.spec\.js$/,
                loader: 'ignore-loader'
            },
            {
                test: /locale\/([a-z]{2})\.json$/,
                loader: 'ignore-loader'
            },
            {
                test: /\.scss$/,
                loader: 'ignore-loader',
                exclude: [/main\.scss/]
            }
        ].concat(isCoverage ? { test: /\.js$/, include: path.resolve('./app'), exclude: /(tests|node_modules|\.spec\.js|main\.js$)/, loader: 'istanbul-instrumenter-loader' }: []),
    },
    devtool: '#inline-cheap-module-source-map',
    resolve: {
        modules: [
            __dirname,
            'node_modules'
        ],
        extensions: ['.js', '.json']
    }
};